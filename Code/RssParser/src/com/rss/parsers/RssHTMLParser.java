package com.rss.parsers;


import java.io.IOException;
import java.util.ArrayList;

import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.rss.components.ItemContent;

public class RssHTMLParser {

	
	
	/*
	 * Lấy nội dung của bài báo bỏ title và description (sẽ được set vào từ nội dung trong RssItem)
	 * Chỉ sử dụng cho cú pháp html báo vnexpress
	 */

	public ItemContent getContentFromHTML(String pageUrl)
	{
		Document doc;
		ArrayList<String> imageLink = new ArrayList<String>();
		ArrayList<String> text = new ArrayList<String>();
		ArrayList<Boolean> isImageDescrip = new ArrayList<Boolean>();
		try {
			doc = Jsoup.connect(pageUrl).get();	
			//lấy tất cả text nằm trong nội dung của mẩu tin
			Elements content = doc.select("p[class=Normal], p[class=Image]");
			//Elements imageDocs = doc.select("p[class=Image]");
			//Elements image = doc.getElementsByTag("img");
			
			for (Element tmp:content)
			{
				text.add(tmp.text());
				// nếu class=Image -> add true - là mô tả ảnh
				isImageDescrip.add(tmp.attr("class").equalsIgnoreCase("Image"));
					
			}
			
			
			//tìm các thẻ có chứa link hình ảnh trong mẩu tin 
			Elements imageUrl = doc.getElementsByAttribute("data-natural-width");	
			for (Element tmp:imageUrl)
            {
            	if (tmp.hasAttr("src"))
            	{
            		imageLink.add(tmp.attr("src"));
            	}
            }
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return new ItemContent(text,isImageDescrip,imageLink);	
	}
}