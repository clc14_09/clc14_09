package com.rss.parsers;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.rss.components.RssItem;

/*
 * Chứa các hàm xử lý rss xml(phân tích xml lấy các item)
 */
public class RssXMLParser {
	private final static String TAG_TITLE = "title";
	private final static String TAG_LINK = "link";
	private final static String TAG_DESCRIPTION = "description";
	private final static String TAG_PUBDATE = "pubDate";
	
	public ArrayList<RssItem> parseXML(String xmlUrl) throws IOException, ParserConfigurationException, SAXException
	{
		ArrayList<RssItem> itemList = new ArrayList<RssItem>();
		//tạo kết nói với url từ tham số truyền vào
		URL url = new URL(xmlUrl);
		HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
		int responseCode = httpConnection.getResponseCode();
		//kết nối thành công, tạo luồng để đọc nội dung
        if (responseCode == HttpURLConnection.HTTP_OK) {
            InputStream in = httpConnection.getInputStream();
            
            //tạo document builder để xử lý luồng vào
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            
            //tạo cây DOM cho luồng XML vào
            Document dom = db.parse(in);
            
            //Phân tích cú pháp cây
            Element treeElements = dom.getDocumentElement();
            NodeList itemNodes = treeElements.getElementsByTagName("item");
            
            //lấy thông tin từng item đưa vào trong list
            if (itemNodes != null) {
                for (int i = 0; i < itemNodes.getLength(); i++) {
                    itemList.add(parseItemNode(itemNodes, i) );
                }// for
            }// if
        }// if
        
        //đóng kết nối 
        httpConnection.disconnect();
		return itemList;
	}

	private RssItem parseItemNode(NodeList itemNodes, int i) throws ParserConfigurationException, SAXException, IOException {
		// TODO Auto-generated method stub
		Element entry = (Element) itemNodes.item(i);
		
		//lấy nội dung các tag từ node con
        Element title = (Element) entry.getElementsByTagName(TAG_TITLE).item(0);
        Element description = (Element) entry.getElementsByTagName(TAG_DESCRIPTION).item(0);
        Element pubDate = (Element) entry.getElementsByTagName(TAG_PUBDATE).item(0);
        Element link = (Element) entry.getElementsByTagName(TAG_LINK).item(0);
        
        String titleValue = title.getFirstChild().getNodeValue();
        String descriptionValue =description.getFirstChild().getNodeValue();
        String dateValue = pubDate.getFirstChild().getNodeValue();
        String linkValue = link.getFirstChild().getNodeValue();
        
        //lấy link image đại diện, và description
        //chi danh cho bao vnexpress
        String descriptionContent = descriptionValue.substring(descriptionValue.lastIndexOf("</br>")+5);
        int start = descriptionValue.lastIndexOf("http");
        int end  = descriptionValue.lastIndexOf(" >");
        String imageUrl = descriptionValue.substring(start, end-1);
        return new RssItem(titleValue,linkValue, descriptionContent, dateValue, imageUrl);
		
	}
}
