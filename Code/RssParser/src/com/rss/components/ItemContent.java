package com.rss.components;

import java.util.ArrayList;

public class ItemContent {
	ArrayList<String> text = new ArrayList<String>();
	ArrayList<Boolean> isImageDescrip;
	ArrayList<String> imageUrl = new ArrayList<String>();
	
	
	public ItemContent(ArrayList<String> text, ArrayList<Boolean> isImageDescrip, ArrayList<String> imageUrl) {
		super();
		this.text = text;
		this.isImageDescrip = isImageDescrip;
		this.imageUrl = imageUrl;
	}

	public ArrayList<String> getText() {
		return text;
	}
	public void setText(ArrayList<String> text) {
		this.text = text;
	}
	public ArrayList<Boolean> getIsImageDescrip() {
		return isImageDescrip;
	}
	public void setIsImageDescrip(ArrayList<Boolean> isImageDescrip) {
		this.isImageDescrip = isImageDescrip;
	}
	public ArrayList<String> getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(ArrayList<String> imageUrl) {
		this.imageUrl = imageUrl;
	}
	@Override
	public String toString() {
		return "ItemContent [" + "text=" + text + ", isImageDescrip="
				+ isImageDescrip + ", imageUrl=" + imageUrl + "]";
	}
	
	
	
}
