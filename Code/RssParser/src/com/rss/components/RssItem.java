package com.rss.components;


public class RssItem {
	String title;
	String link;
	String description;
	String date;
	String imageUrl;
	
	
	


	@Override
	public String toString() {
		return "RssItem [title=" + title + ", link=" + link + ", description=" + description + ", date=" + date
				+ ", imageUrl=" + imageUrl + "]";
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String image) {
		this.imageUrl = image;
	}
	public RssItem(String title, String link, String description, String date, String imageUrl) {
		super();
		this.title = title;
		this.link = link;
		this.description = description;
		this.date = date;
		this.imageUrl = imageUrl;
	}
	public RssItem() {
		super();
	}

	
	
}