package com.example.beou.rssreadervnexpress.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.beou.rssreadervnexpress.R;

public class UserGuildActivity extends Activity {

    ImageView imgGuild;
    Button btnNext;
    int image[]={R.drawable.ic_refresh1,R.drawable.ic_menu2,R.drawable.ic_choose_topic3
            ,R.drawable.ic_choose_setting4,R.drawable.ic_choose_bookmark5,R.drawable.ic_bookmark6
            ,R.drawable.ic_user_guild7};
    int curIndex = 0;
    int end = image.length-1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_guild);
        addControl();
        imgGuild.setImageResource(image[0]);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (curIndex!=end){
                    curIndex++;
                    imgGuild.setImageResource(image[curIndex]);
                    if (curIndex==end){
                        btnNext.setText("Hoàn thành");
                    }
                }
                else {

                    onBackPressed();
                }
            }
        });

    }

    private void addControl() {
        imgGuild = (ImageView) findViewById(R.id.imgGuild);
        btnNext = (Button) findViewById(R.id.btnNext);
    }
}
