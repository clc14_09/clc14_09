package com.example.beou.rssreadervnexpress.service;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.beou.rssreadervnexpress.parsers.HotNewsParser;
import com.example.beou.rssreadervnexpress.variable.Variable;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class HotNewsService extends Service {
    HotNewsParser hotNews;
    LocalBroadcastManager broadcaster;


    ProgressDialog progressDialog;
    public HotNewsService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                if (isOnline()==true){
                    Variable.isSaveDone = false;
                    //progressDialog.show();
                    //Toast.makeText(HotNewsService.this, "", Toast.LENGTH_SHORT).show();
                    Log.d("State123","Online");
                    hotNews= new HotNewsParser(getBaseContext());
                    hotNews.execute();
//                    if (hotNews.getStatus().equals(AsyncTask.Status.FINISHED)){
//                        sendResult("abc");
//                    }
                    //Toast.makeText(getApplication(), "Online", Toast.LENGTH_SHORT).show();
                    hotNews = null;
                    while(Variable.isSaveDone == false) {

                    }
                    // broadcaster = LocalBroadcastManager.getInstance(getApplicationContext());
                    //sendResult("abc");
                    //gửi yêu cầu đồng bộ UI
                    if(isForeground("com.example.beou.rssreadervnexpress") == true) {
                        Log.d("abc","is Foreground");
                        broadcaster = LocalBroadcastManager.getInstance(getApplicationContext());
                        sendResult("abc");
                    }
                    else {
                        Log.d("abc","is NOT Foreground");
                    }

//                    Variable.isSaveDone = false;

                }

            }
        };
        //new Timer().schedule(timerTask,0,600000);
        new Timer().schedule(timerTask,0,600000);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    static final public String REQUEST = "REQUEST_PROCESSED";

    static final public String MESSAGE = "MESSAGE";

    public void sendResult(String message) {
        Intent intent = new Intent(REQUEST);
        if(message != null)
            intent.putExtra(MESSAGE, message);

        broadcaster.sendBroadcast(intent);
    }

    public boolean isForeground(String myPackage) {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1);
        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;
        return componentInfo.getPackageName().equals(myPackage);
    }
}
