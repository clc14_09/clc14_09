package com.example.beou.rssreadervnexpress.parsers;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.beou.rssreadervnexpress.components.NewsContent;
import com.example.beou.rssreadervnexpress.variable.Variable;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

/*
 * Chứa các hàm xử lý html (lấy nội dung báo của link html đưa vào)
 */
public class RssHTMLParser extends AsyncTask<String,Void,Void>{
    LinearLayout mainlayout;
    Context context;
    ProgressDialog progressDialog;
    NewsContent newsContent=null;
	/*
	 * Lấy nội dung của bài báo bỏ title và description (sẽ được set vào từ nội dung trong RssItem)
	 * Chỉ sử dụng cho cú pháp html báo vnexpress
	 */

    public RssHTMLParser(Context context, LinearLayout mainlayout) {
        this.context = context;
        this.mainlayout = mainlayout;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.show();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        progressDialog.dismiss();
        //itemContent.display(context,mainlayout);

        if (newsContent==null){
            Toast.makeText(context, "Khong the load noi dung", Toast.LENGTH_SHORT).show();
        }
        else{
            //TODO: xử lý sự kiện online offine ở đây - bỏ
            newsContent.displayOnl(context, mainlayout);
            //Variable.newsContent = this.newsContent;
        }

    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected Void doInBackground(String... params) {
        //itemContent = getContentFromHTML(params[0]);
        newsContent = getContentFromHTML(params[0]);
        return null;
    }

    public NewsContent getContentFromHTML(String pageUrl)
    {
        Document doc;
        String title=null;
        String subTitle =null;
        String date=null;
        ArrayList<String> string = new ArrayList<String>();

        //0: text thuong, 1: link img, 2: text mo ta hinh
        ArrayList<Integer> type = new ArrayList<Integer>();
        try {
            doc = Jsoup.connect(pageUrl).get();
            //lấy tất cả text nằm trong nội dung của mẩu tin

            if (doc.select("[class=block_timer left txt_666]").size()>=2) {
                date = doc.select("[class=block_timer left txt_666]").get(1).text();
            }
            //Toast.makeText(context, doc.select("[class=block_timer left txt_666]").size()+"", Toast.LENGTH_SHORT).show();
            if (doc.select("[class=short_intro txt_666]").size()!=0) {
                subTitle = doc.select("[class=short_intro txt_666]").get(0).text();
            }
            //title = doc.select("[class=short_intro txt_666]").get(0).text();
            if(doc.select("[class=title_news]").size()!=0) {
                title = doc.select("[class=title_news]").get(0).text();
            }
            Elements content = doc.select("div[class=main_content_detail width_common] p, div[class=main_content_detail width_common] img");

            for (Element tmp:content)
            {
                if (tmp.nodeName().equals("img")){
                    //System.out.println(tmp.toString());
                    if ((tmp.hasAttr("data-natural-width"))
                            ||(tmp.hasAttr("data-reference-id"))
                            ||(tmp.hasAttr("data-width"))) {
                        string.add(tmp.attr("src"));
                        type.add(Variable.IMG_LINK);
                    }

                }

                else if (tmp.nodeName().equals("p")){
                    //if(tmp.parent().select("div[class=desc_cation]").toString();
                    //System.out.println(tmp.parent().select("div[class=desc_cation]"));

                    if (tmp.attr("class").equals("Image")){
                        string.add(tmp.text());
                        type.add(Variable.IMG_TEXT);
                    }
                    else {
                        int count = 0;
                        Elements parents = tmp.parent().select("div[class=desc_cation]");
                        for (Element tmp1:parents)
                        {
                            count++;
                            if (tmp1.hasAttr("style")){
                                if (!tmp.text().equals(string.get(string.size()-1))){
                                    string.add(tmp.text());
                                    type.add(Variable.NORMAL_TEXT);
                                }
                            }
                            else{
                                string.add(tmp.text());
                                type.add(Variable.IMG_TEXT);
                            }
                        }
                        if (count == 0){
                            string.add(tmp.text());
                            type.add(Variable.NORMAL_TEXT);
                        }
                    }
                }
//                content = doc.select("div[fck_detail width_common block_ads_connect]");
//                string.add(content.size()+"");
//                for (Element tmp1:content){
//                    string.add(content.toString());
//                    type.add(Variable.NORMAL_TEXT);
//                }
//                if (content.size() == 0){
//                    //Toast.makeText(context, "lala", Toast.LENGTH_SHORT).show();
//                    Log.d("LALA","NULL");
//
//                }
            }


        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
        return new NewsContent(title, subTitle,date,string,type);
    }
}
