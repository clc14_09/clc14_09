package com.example.beou.rssreadervnexpress.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.beou.rssreadervnexpress.R;
import com.example.beou.rssreadervnexpress.database.BookMarkDB;

import java.util.ArrayList;

/**
 * Created by Quang Tu on 12/28/2016.
 */

public class BookmarkAdapter extends BaseAdapter implements ListAdapter {
    private Context context;
    BookMarkDB DB;


    public BookmarkAdapter(BookMarkDB db, Context context) {
        DB= db;
        this.context = context;
    }

    @Override
    public int getCount() {
        return DB.GetNumArray();
    }

    @Override
    public Object getItem(int pos) {
        return DB.GetTitle(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;
        //just return 0 if your list items do not have an Id variable.
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.bookmark_item, null);
        }

        //Handle TextView and display string from your list
        TextView listItemText = (TextView)view.findViewById(R.id.list_item_string);
        listItemText.setText(DB.GetTitle(position));

        //Handle buttons and add onClickListeners
        Button deleteBtn = (Button)view.findViewById(R.id.btndelete);

        deleteBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //do something
                DB.Delete(position);
                notifyDataSetChanged();
            }
        });

        return view;
    }
}
