package com.example.beou.rssreadervnexpress.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Toast;

import com.example.beou.rssreadervnexpress.R;
import com.example.beou.rssreadervnexpress.adapter.DrawerAdapter;
import com.example.beou.rssreadervnexpress.service.HotNewsService;
import com.example.beou.rssreadervnexpress.variable.Variable;

import java.util.Arrays;

public class MainActivity extends Activity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    static  int selected = Variable.HOT_NEWS;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] drawerStrItems;
    Intent hotNewsService;
    private SeekBar sdTextsize;
    private int textsizeRatio = 0;
    private Button btnWhite, btnGrey, btnYellow;
    private SharedPreferences mSharedPreferences;
    private static String bgColor = "#ffffff";
    //static Timer  timer;

    boolean isFirst = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //mSharedPreferences = this.getSharedPreferences(getString(R.string.shared_setting), MODE_PRIVATE);
        //Variable.backgroundColor = mSharedPreferences.getString(getString(R.string.background_color), "ffffff");


        if (isMyServiceRunning(HotNewsService.class)== false) {
//            Toast.makeText(this, "MyService IS NOT running", Toast.LENGTH_SHORT).show();
            hotNewsService = new Intent(this, HotNewsService.class);
            startService(hotNewsService);
        }

        //xác định có phải lần đầu mở app để hiện hướng dẫn
        SharedPreferences settings = getSharedPreferences("FirstTimeFile", 0);

        if (settings.getBoolean("getFirst", true)) {
            startActivity(new Intent(getApplication(),UserGuildActivity.class));
            settings.edit().putBoolean("getFirst", false).commit();
        }





        mTitle = mDrawerTitle = getTitle();
        drawerStrItems = Variable.TOPIC;
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        DrawerAdapter drawerAdapter = new DrawerAdapter(this,R.layout.drawer_list_item, Arrays.asList(Variable.TOPIC));
        mDrawerList.setAdapter(drawerAdapter);

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);


        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                //getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {

                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            selectItem(Variable.HOT_NEWS);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_reload).setVisible(!drawerOpen);
        if (selected == Variable.HOT_NEWS || selected == Variable.USER_GUILD
                || selected==Variable.BOOKMARK || selected == Variable.SETTING){
            menu.findItem(R.id.action_reload).setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        timer.cancel();
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch(item.getItemId()) {
            case R.id.action_reload:

                //topic: hotNews
//                if (selected == 0){
//                    if (isOnline() == true){
//                        //khi service trc đã save x
//                        //if (Variable.isSaveDone == true) {
//                            stopService(hotNewsService);
//                            startService(hotNewsService);
//                        //}
//                    }
//                    else{
//                        Toast.makeText(this, "Không thể kết nối Internet"
//                                , Toast.LENGTH_SHORT).show();
//                    }
//                }
                //topic: lấy bên Rss
                if (isOnline() == true){
                    selectItem(selected);
                }
                else{
                    Toast.makeText(this, "Không thể kết nối Internet\nChuyển sang chế độ offline"
                            , Toast.LENGTH_SHORT).show();
                    selectItem(Variable.HOT_NEWS);
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            // timer.cancel();

            selected = position;
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        selected = position;

//        timer.cancel();
        Fragment fragment = null;
        // update the main content by replacing fragments
        if(position==Variable.HOT_NEWS){
            getActionBar().setSubtitle(drawerStrItems[position]);
            invalidateOptionsMenu();
            //khi service trc đã save x
            //if (Variable.isSaveDone == true) {
//                    stopService(hotNewsService);
//                    startService(hotNewsService);
            //}
            fragment = new HotNewsFragment();
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
        }
        else{
            if(position == Variable.SETTING){ //setting
                showCustomDialogBox();
            }
            else if (position == Variable.USER_GUILD){
                //Intent intentGuild = new Intent(getApplication(),UserGuildActivity.class);
                startActivity(new Intent(getApplication(),UserGuildActivity.class));
            }
            else if (position == Variable.BOOKMARK){
                startActivity(new Intent(this,BookmarkActivity.class));
            }
            else {
                if (isOnline() == false) {
                    Toast.makeText(this, "Không thể kết nối Internet\nChuyển sang chế độ đọc offline"
                            , Toast.LENGTH_SHORT).show();
                    selected = Variable.HOT_NEWS;
                    selectItem(Variable.HOT_NEWS);
                } else {
                    fragment = new RssFragment();
                    Bundle args = new Bundle();
                    args.putInt(RssFragment.RSS_POSITION, position);
                    fragment.setArguments(args);
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                }
            }
        }


        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        if (position!= Variable.USER_GUILD&& position!=Variable.BOOKMARK && position!=Variable.SETTING) {
            getActionBar().setSubtitle(drawerStrItems[position]);
        }
        mDrawerLayout.closeDrawer(mDrawerList);
    }


    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void showCustomDialogBox() {
        final Dialog customDialog = new Dialog(this);
        final LayoutInflater Layout = getLayoutInflater();
        final View hotnews = Layout.inflate(R.layout.hot_news, null);
        final View rssItem = Layout.inflate(R.layout.rss_item, null);
        final View articleContent = Layout.inflate(R.layout.item_content_activity, null);

        customDialog.setTitle("Setting");
// match customDialog with custom dialog layout
        customDialog.setContentView(R.layout.setting_dialog);
        sdTextsize = (SeekBar) customDialog.findViewById(R.id.sd_textsize);
        sdTextsize.setProgress(textsizeRatio);
        sdTextsize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textsizeRatio = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        btnWhite = (Button) customDialog.findViewById(R.id. colorWhite);
        btnGrey = (Button) customDialog.findViewById(R.id. colorGrey);
        btnYellow = (Button) customDialog.findViewById(R.id. colorYellow);

        btnWhite. setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bgColor = "#ffffff";
                GradientDrawable gd = new GradientDrawable();
                gd.setColor(Color.parseColor("#ffffff"));
                gd.setStroke(6, 0xFF000000);
                btnWhite.setBackgroundDrawable(gd);
                btnGrey.setBackgroundColor(Color.parseColor("#e0e0e0"));
                btnYellow.setBackgroundColor(Color.parseColor("#fff9c4"));
            }
        });

        btnGrey. setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bgColor = "#e0e0e0";
                        GradientDrawable gd = new GradientDrawable();
                        gd.setColor(Color.parseColor("#e0e0e0"));
                        gd.setStroke(6, 0xFF000000);
                        btnGrey.setBackgroundDrawable(gd);
                        btnWhite.setBackgroundColor(Color.parseColor("#ffffff"));
                        btnYellow.setBackgroundColor(Color.parseColor("#fff9c4"));
                    }
                });

        btnYellow. setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bgColor = "#fff9c4";
                        GradientDrawable gd = new GradientDrawable();
                        gd.setColor(Color.parseColor("#fff9c4"));
                        gd.setStroke(6, 0xFF000000);
                        btnYellow.setBackgroundDrawable(gd);
                        btnWhite.setBackgroundColor(Color.parseColor("#ffffff"));
                        btnGrey.setBackgroundColor(Color.parseColor("#e0e0e0"));
                    }
                });
        ((Button) customDialog.findViewById(R.id. sd_btnok))
                . setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Variable.backgroundColor = bgColor;
                        View xxx = findViewById(R.id.content_frame);
                        View root = xxx.getRootView();
                        root.setBackgroundColor(Color.parseColor(Variable.backgroundColor));

                        Variable.titleTextSize = Variable.dftitleTextSize + textsizeRatio;
                        Variable.subTitleTextSize = Variable.dfsubTitleTextSize + textsizeRatio;
                        Variable.dateTextSize = Variable.dfdateTextSize + textsizeRatio;
                        Variable.normalTextSize = Variable.dfnormalTextSize + textsizeRatio;
                        Variable.imgdescTextSize = Variable.dfimgdescTextSize + textsizeRatio;

                        Log. e("onChangecolor", Variable.backgroundColor);
                        customDialog.dismiss();
                    };
                });
        ((Button) customDialog.findViewById(R.id. sd_btnCancel))
                . setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customDialog.dismiss();
                    }
                });
        customDialog.show();
    }

}
