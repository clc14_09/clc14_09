package com.example.beou.rssreadervnexpress.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.beou.rssreadervnexpress.R;
import com.example.beou.rssreadervnexpress.variable.Variable;

import java.util.List;



public class DrawerAdapter extends ArrayAdapter<String> {

    Context context;
    public DrawerAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
//        if (convertView == null)
//        {
//            convertView = LayoutInflater.from(getContext()).inflate(R.layout.drawer_list_item,parent,false);
//            //imgView.setImageResource(R.drawable.logo);
//        }
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.drawer_list_item,parent,false);
        if (position==2)
        {
            TextView txtLine = (TextView) convertView.findViewById(R.id.txtLine);
            txtLine.setBackgroundResource(R.color.white);
        }
        ImageView imgView = (ImageView) convertView.findViewById(R.id.imgItem);
        if (position != Variable.USER_GUILD) {
            imgView.setImageResource(Variable.menuIcon[position]);
        }
        TextView txtItem = (TextView) convertView.findViewById(R.id.txtItem);
        txtItem.setText(Variable.TOPIC[position]);

        return convertView;
    }
}
