package com.example.beou.rssreadervnexpress.components;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.beou.rssreadervnexpress.R;
import com.example.beou.rssreadervnexpress.variable.Variable;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by BeoU on 12/4/2016.
 */

public class NewsContent implements Serializable{
    String title=null;
    String subTitle =null;
    String date=null;

    //array string và type có chung size
    ArrayList<String> string= new ArrayList<String>();; // array chứa chuỗi
    ArrayList<Integer> type = new ArrayList<Integer>();// array chứa kiểu chuỗi: link hình, chú thích hình hay nội dung
    HashMap<Integer,Bitmap> image = new HashMap<Integer,Bitmap>();
    LinearLayout.LayoutParams layoutParamsText = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    LinearLayout.LayoutParams layoutParamsImg = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT
            , LinearLayout.LayoutParams.MATCH_PARENT+700);
    public NewsContent() {
    }

    public NewsContent(String title, String subTitle, String date, ArrayList<String> string, ArrayList<Integer> type) {
        this.title = title;
        this.subTitle = subTitle;
        this.date = date;
        this.string = string;
        this.type = type;
    }

    public void displayText(String text, int textSize, String style,Context context, LinearLayout mainlayout, int index){
        TextView txtNews = new TextView(context);
        txtNews.setLayoutParams(layoutParamsText);
        txtNews.setText(text);
        txtNews.setTextSize(textSize);

        if (index == -1){
        }
        else{
            if (type.get(index)==Variable.IMG_TEXT){
                txtNews.setBackgroundColor(0xffcccccc);
                //txtNews.setBackgroundColor(0xFF00FF00);
            }
        }
        mainlayout.addView(txtNews);
    }
    public void displayImgUrl(String url,Context context, LinearLayout mainlayout){
        ImageView imageView = new ImageView(context);

        Picasso.with(context)
                .load(url)
                .placeholder(R.drawable.loading)
                .error(R.drawable.error)
                .into(imageView);
        imageView.setLayoutParams(layoutParamsImg);
        mainlayout.addView(imageView);
    }
    public void displayImgBitmap(Bitmap bitmap,Context context, LinearLayout mainlayout){
        ImageView imageView = new ImageView(context);

//        Picasso.with(context)
//                .load(bitmap)
//                .placeholder(R.drawable.loading)
//                .error(R.drawable.error)
//                .into(imageView);
        imageView.setImageBitmap(bitmap);
        imageView.setLayoutParams(layoutParamsImg);
        mainlayout.addView(imageView);
    }

    public void displayOnl(Context context, LinearLayout mainlayout)
    {
        displayText(date,Variable.dateTextSize,null,context,mainlayout,-1);
        displayText(title,Variable.titleTextSize,null,context,mainlayout,-1);
        displayText(subTitle,Variable.subTitleTextSize,null,context,mainlayout,-1);
        for (int i=0; i<string.size(); i++) {
            if(type.get(i) == Variable.IMG_LINK){
                displayImgUrl(string.get(i),context,mainlayout);
            }
            if(type.get(i) == Variable.IMG_TEXT){
                displayText(string.get(i),Variable.imgdescTextSize,null,context,mainlayout, i);
            }
            else if(type.get(i) == Variable.NORMAL_TEXT){
                displayText(string.get(i),Variable.normalTextSize,null,context,mainlayout,i);
            }
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<String> getString() {
        return string;
    }

    public void setString(ArrayList<String> string) {
        this.string = string;
    }

    public ArrayList<Integer> getType() {
        return type;
    }

    public void setType(ArrayList<Integer> type) {
        this.type = type;
    }

    public Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    public void saveInternal(String fileName, Context context) {
//        String title=null;
//        String subTitle =null;
//        String date=null;
//        ArrayList<String> string;
//        ArrayList<Integer> type = new ArrayList<Integer>();

        File file;
        FileOutputStream outputStream;
        try {
            file = new File(context.getFilesDir(), fileName);
            outputStream = new FileOutputStream(file);

            outputStream.write((title+"\n").getBytes());
            outputStream.write((subTitle+"\n").getBytes());
            outputStream.write((date+"\n").getBytes());

            //Lưu array type
            outputStream.write((type.size()+"\n").getBytes()); //luu so luong phần từ trong dãy
            for (int i=0; i<type.size(); i++){
                outputStream.write((type.get(i)+"\n").getBytes());
            }


            //luu array string
            //outputStream.write((string.size()+"\n").getBytes());
            for (int i=0; i<string.size(); i++) {
                if(type.get(i) == Variable.IMG_LINK){
                    // luu thanh file hinh với tên file là fileName+"_image"+index(index là vi tri trong mảng)
                    String imageFileName = fileName+"_image"+i;
                    saveBitmap2File(imageFileName,getBitmapFromURL(string.get(i)),context);

                }
                else{
                    outputStream.write((string.get(i)+"\n").getBytes());
                }
            }

            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();

        }

    }
    public void readInternal(String fileName, Context context){
        BufferedReader input = null;
        File file = null;
        try {
            file = new File(context.getFilesDir(), fileName); // Pass getFilesDir() and "MyFile" to read file

            input = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String line;

            line = input.readLine();
            if (line == null){
                return;
            }
            title = line;
            subTitle= input.readLine();
            date= input.readLine();
            int size = Integer.parseInt(input.readLine());


//            link= input.readLine();
//            int size = Integer.parseInt(input.readLine());
            for (int i=0;i<size;i++){
                type.add(Integer.parseInt(input.readLine()));
            }
            for (int i=0;i<size;i++){
                if(type.get(i) == Variable.IMG_LINK){
                    // đọc bitmap từ fileName+"_image"+index(index là vi tri trong mảng)
                    String imageFileName = fileName+"_image"+i;
                    //saveBitmap2File(imageFileName,getBitmapFromURL(string.get(i)),context);
                    image.put(i,readBitmapFromFile(imageFileName,context));
                    string.add("image");
                }
                else{
                    string.add(input.readLine());
                }
            }
            input.close();
        } catch (IOException e) {
            e.printStackTrace();

        }


        //image.put(index, b);
    }


    public void saveBitmap2File(String filename, Bitmap bmp, Context context){
        File file;
        FileOutputStream out;
        try {
            file = new File(context.getFilesDir(), filename);
            out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap readBitmapFromFile(String filename, Context context){
        File file;
        FileInputStream fis;

        try{
            file = new File(context.getFilesDir(), filename);
            fis = new FileInputStream(file);
            Bitmap b = BitmapFactory.decodeStream(fis);

            fis.close();
            return b;

        }
        catch(Exception e){
            e.printStackTrace();
            Log.e("Read", "read bitmap");
            return  null;
        }

    }

    //TODO: tim cach luu img offline de load len, img luu duoi dang bitmap

    public void displayOffline(String fileName, Context context, LinearLayout mainlayout){
        readInternal(fileName,context);
        displayText(date,Variable.dateTextSize,null,context,mainlayout,-1);
        displayText(title,Variable.titleTextSize,null,context,mainlayout,-1);
        displayText(subTitle,Variable.subTitleTextSize,null,context,mainlayout,-1);
        for (int i=0; i<string.size(); i++) {
            if(type.get(i) == Variable.IMG_LINK){
                // displayImgUrl(string.get(i),context,mainlayout);
                displayImgBitmap(image.get(i),context,mainlayout);
            }
            if(type.get(i) == Variable.IMG_TEXT){
                displayText(string.get(i),Variable.imgdescTextSize,null,context,mainlayout, i);
            }
            else if(type.get(i) == Variable.NORMAL_TEXT){
                displayText(string.get(i),Variable.normalTextSize,null,context,mainlayout,i);
            }
        }
    }
}
