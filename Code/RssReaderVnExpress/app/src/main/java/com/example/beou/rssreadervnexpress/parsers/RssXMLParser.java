package com.example.beou.rssreadervnexpress.parsers;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.beou.rssreadervnexpress.adapter.RssAdapter;
import com.example.beou.rssreadervnexpress.components.RssItem;
import com.example.beou.rssreadervnexpress.variable.Variable;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/*
 * Chứa các hàm xử lý rss xml(phân tích xml lấy các item)
 */
public class RssXMLParser extends AsyncTask<Void, Void, Void> {
    private final static String TAG_TITLE = "title";
    private final static String TAG_LINK = "link";
    private final static String TAG_DESCRIPTION = "description";
    private final static String TAG_PUBDATE = "pubDate";

    Context context;
    //String address = "http://www.sciencemag.org/rss/news_current.xml";
    ProgressDialog progressDialog;
    ArrayList<RssItem> rssItem;
    RecyclerView recyclerView;
    int index;
    public RssXMLParser(int index,Context context,RecyclerView recyclerView) {
        this.index = index;
        this.recyclerView=recyclerView;
        this.context = context;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
    }

    @Override
    protected void onPreExecute() {
        progressDialog.show();
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        progressDialog.dismiss();
        RssAdapter adapter=new RssAdapter(context,rssItem);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        //recyclerView.addItemDecoration(new VerticalSpace(50));
        recyclerView.setAdapter(adapter);

    }

    @Override
    protected Void doInBackground(Void... params) {
        //ProcessXml(Getdata());
        try {
            rssItem = parseXML(Variable.RSS_LINK[index]);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        return null;
    }
    public ArrayList<RssItem> parseXML(String xmlUrl) throws IOException, ParserConfigurationException, SAXException
    {
        ArrayList<RssItem> itemList = new ArrayList<RssItem>();
        //tạo kết nói với url từ tham số truyền vào
        URL url = new URL(xmlUrl);
        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
        int responseCode = httpConnection.getResponseCode();
        //kết nối thành công, tạo luồng để đọc nội dung
        if (responseCode == HttpURLConnection.HTTP_OK) {
            InputStream in = httpConnection.getInputStream();

            //tạo document builder để xử lý luồng vào
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();

            //tạo cây DOM cho luồng XML vào
            Document dom = db.parse(in);

            //Phân tích cú pháp cây
            Element treeElements = dom.getDocumentElement();
            NodeList itemNodes = treeElements.getElementsByTagName("item");

            //lấy thông tin từng item đưa vào trong list
            if (itemNodes != null) {
                for (int i = 0; i < itemNodes.getLength(); i++) {
                    itemList.add(parseItemNode(itemNodes, i) );
                }// for
            }// if
        }// if

        //đóng kết nối
        httpConnection.disconnect();
        return itemList;
    }

    private RssItem parseItemNode(NodeList itemNodes, int i) throws ParserConfigurationException, SAXException, IOException {
        // TODO Auto-generated method stub
        Element entry = (Element) itemNodes.item(i);

        //lấy nội dung các tag từ node con
        Element title = (Element) entry.getElementsByTagName(TAG_TITLE).item(0);
        Element description = (Element) entry.getElementsByTagName(TAG_DESCRIPTION).item(0);
        Element pubDate = (Element) entry.getElementsByTagName(TAG_PUBDATE).item(0);
        Element link = (Element) entry.getElementsByTagName(TAG_LINK).item(0);

        String titleValue = title.getFirstChild().getNodeValue();
        String descriptionValue =description.getFirstChild().getNodeValue();
        String dateValue = pubDate.getFirstChild().getNodeValue();
        String linkValue = link.getFirstChild().getNodeValue();

        //lấy link image đại diện, và description
        //chi danh cho bao vnexpress
        String descriptionContent = descriptionValue.substring(descriptionValue.lastIndexOf("</br>")+5);

        //TODO: Sua lai cach lay link anh
        int start = descriptionValue.lastIndexOf("http");
        int end  = descriptionValue.lastIndexOf(" >");
        String imageUrl;
        if (start== -1 || end == -1 || start>=end){
            imageUrl = null;
        }
        else {
            imageUrl = descriptionValue.substring(start, end-1);
        }
        return new RssItem(titleValue,linkValue, descriptionContent, dateValue, imageUrl);

    }
}
