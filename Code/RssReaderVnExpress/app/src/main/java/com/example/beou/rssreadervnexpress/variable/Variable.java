package com.example.beou.rssreadervnexpress.variable;

import com.example.beou.rssreadervnexpress.R;

/**
 * Created by BeoU on 11/27/2016.
 */

public class Variable {
//    public final static String[] TOPIC = {"Hot News","Trang chủ","Thời sự","Thế giới","Kinh doanh","Giải trí",
//            "Thể thao", "Pháp luật","Giáo dục","Sức khỏe", "Setting", "Bookmark", "Hướng dẫn sử dụng"};
//    public final static String[] RSS_LINK ={"",
//            "http://vnexpress.net/rss/tin-moi-nhat.rss",
//            "http://vnexpress.net/rss/thoi-su.rss",
//            "http://vnexpress.net/rss/the-gioi.rss",
//            "http://vnexpress.net/rss/kinh-doanh.rss",
//            "http://vnexpress.net/rss/giai-tri.rss",
//            "http://vnexpress.net/rss/the-thao.rss",
//            "http://vnexpress.net/rss/phap-luat.rss",
//            "http://vnexpress.net/rss/giao-duc.rss",
//            "http://vnexpress.net/rss/suc-khoe.rss"};
    public final static String[] TOPIC = {"Hướng dẫn sử dụng", "Setting", "Bookmark","Hot News"
        ,"Trang chủ","Thời sự","Thế giới","Kinh doanh","Giải trí",
        "Thể thao", "Pháp luật","Giáo dục","Sức khỏe"};
    public final static int menuIcon[] = {0, R.drawable.ic_settings,R.drawable.ic_bookmarkmenu
    ,R.drawable.ic_hotnews,R.drawable.ic_trangchu,R.drawable.ic_thoisu,R.drawable.ic_thegioi
    ,R.drawable.ic_kinhdoanh,R.drawable.ic_giaitri,R.drawable.ic_thethao,R.drawable.ic_phapluat
    ,R.drawable.ic_giaoduc,R.drawable.ic_suckhoe};
    public final static String[] RSS_LINK ={"","","","",
            "http://vnexpress.net/rss/tin-moi-nhat.rss",
            "http://vnexpress.net/rss/thoi-su.rss",
            "http://vnexpress.net/rss/the-gioi.rss",
            "http://vnexpress.net/rss/kinh-doanh.rss",
            "http://vnexpress.net/rss/giai-tri.rss",
            "http://vnexpress.net/rss/the-thao.rss",
            "http://vnexpress.net/rss/phap-luat.rss",
            "http://vnexpress.net/rss/giao-duc.rss",
            "http://vnexpress.net/rss/suc-khoe.rss"};
    public static String fileName = "hot_news";
    public static String fileNameListNewsContent = "list_content_news";
    final public static int NORMAL_TEXT = 0;
    final public static int IMG_LINK = 1;
    final public static int IMG_TEXT = 2;
    public static int titleTextSize =30;
    public static int subTitleTextSize =24;
    public static int dateTextSize =18;
    public static int normalTextSize =22;
    public static int imgdescTextSize =20;

    public static int dftitleTextSize =30;
    public static int dfsubTitleTextSize =24;
    public static int dfdateTextSize =18;
    public static int dfnormalTextSize =22;
    public static int dfimgdescTextSize =20;

    public static String backgroundColor = "#ffffff";
    public static boolean isSaveDone = false;

    public static int BOOKMARK = 2;
    public static int SETTING = 1;
    public static int USER_GUILD = 0;
    public static int HOT_NEWS = 3;
}
