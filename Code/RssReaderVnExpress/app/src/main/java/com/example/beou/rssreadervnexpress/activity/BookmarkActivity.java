package com.example.beou.rssreadervnexpress.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.beou.rssreadervnexpress.R;
import com.example.beou.rssreadervnexpress.adapter.BookmarkAdapter;
import com.example.beou.rssreadervnexpress.database.BookMarkDB;
import com.example.beou.rssreadervnexpress.variable.Variable;

/**
 * Created by Quang Tu on 12/28/2016.
 */

public class BookmarkActivity extends Activity {
    BookMarkDB DB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark);
        DB = new BookMarkDB(this);
        //Bỏ phần này nếu đã có nút bookmark
        //**********************************
        //DB.Insert("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","http://vnexpress.net/tin-tuc/the-gioi/tau-san-bay-lieu-ninh-cap-cang-dao-hai-nam-3520284.html");
        //DB.Insert("bbbbbbbbbbbbbbbbbb","http://vnexpress.net/tong-thuat/the-gioi/may-bay-quan-su-nga-cho-92-nguoi-roi-xuong-bien-den-khong-ai-song-sot-3518855.html");
        //**********************************
        BookmarkAdapter adapter = new BookmarkAdapter (DB,this);

        //handle listview and assign adapter
        ListView lView = (ListView)findViewById(R.id.bookmarklistview);
        lView.setBackgroundColor(Color.parseColor(Variable.backgroundColor));
        lView.setAdapter(adapter);
        lView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(view.getContext(), ItemContentActivity.class);
                intent.putExtra("Link",DB.GetLink(position));
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onDestroy() {
        DB.Save();
        super.onDestroy();
    }
}
