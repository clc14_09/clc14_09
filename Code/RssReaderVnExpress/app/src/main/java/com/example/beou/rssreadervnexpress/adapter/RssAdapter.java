package com.example.beou.rssreadervnexpress.adapter;

/**
 * Created by BeoU on 11/28/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.beou.rssreadervnexpress.R;
import com.example.beou.rssreadervnexpress.activity.ItemContentActivity;
import com.example.beou.rssreadervnexpress.components.RssItem;
import com.example.beou.rssreadervnexpress.variable.Variable;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class RssAdapter extends RecyclerView.Adapter<RssAdapter.MyViewHolder> {
    ArrayList<RssItem>rssItem = new ArrayList<RssItem>();
    Context context;

    public RssAdapter(Context context,ArrayList<RssItem>rssItem){
        if(rssItem!=null) {
            this.rssItem = rssItem;
        }
        this.context=context;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.rss_item,parent,false);
        CardView cardView = (CardView) view.findViewById(R.id.cardview);
        cardView.setBackgroundColor(Color.parseColor(Variable.backgroundColor));
        MyViewHolder holder=new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RssItem current=rssItem.get(position);
        holder.Title.setText(current.getTitle());
        holder.Description.setText(current.getDescription());
        holder.Date.setText(current.getDate());
//        Picasso.with(context).load(current.getImageUrl()).into(holder.Thumbnail);
        Picasso.with(context)
                .load(current.getImageUrl())
                .placeholder(R.drawable.loading)
                .error(R.drawable.error)
                .into(holder.Thumbnail);
        final int index = position;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ItemContentActivity.class);
                intent.putExtra("Link",rssItem.get(index).getLink());
                intent.putExtra("Title",rssItem.get(index).getTitle());
                context.startActivity(intent);
            }
        });
    }



    @Override
    public int getItemCount() {
        return rssItem.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView Title,Description,Date;
        ImageView Thumbnail;
        CardView cardView;
        public MyViewHolder(View itemView) {
            super(itemView);
            Title= (TextView) itemView.findViewById(R.id.txtTitle);
            Description= (TextView) itemView.findViewById(R.id.txtDescription);
            Date= (TextView) itemView.findViewById(R.id.date_text);
            Thumbnail= (ImageView) itemView.findViewById(R.id.imgNews);
            cardView= (CardView) itemView.findViewById(R.id.cardview);
        }
    }
}
