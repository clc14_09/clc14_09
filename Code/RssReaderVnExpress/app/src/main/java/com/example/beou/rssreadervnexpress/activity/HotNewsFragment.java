package com.example.beou.rssreadervnexpress.activity;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beou.rssreadervnexpress.R;
import com.example.beou.rssreadervnexpress.components.HotNews;
import com.example.beou.rssreadervnexpress.service.HotNewsService;
import com.example.beou.rssreadervnexpress.variable.Variable;
import com.squareup.picasso.Picasso;

import static com.example.beou.rssreadervnexpress.variable.Variable.fileName;



public  class HotNewsFragment extends Fragment {
    public static final String RSS_POSITION = "rssPos";
    ListView listView;
    HotNews hotNews = new HotNews();
    ImageView imgNews;
    TextView txtTitle,txtDescription;
    CardView cardView;
    BroadcastReceiver receiver;
    ProgressDialog progressDialog;
    ArrayAdapter<String> adapter;
    private Handler handler = new Handler();
    private static LayoutInflater thisinflater;
    private static ViewGroup thiscontainer;
    public HotNewsFragment() {
        // Empty constructor required for fragment subclasses
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.hot_news, container, false);
        thisinflater = inflater;
        thiscontainer = container;
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        txtTitle = (TextView) rootView.findViewById(R.id.txtTitle);
        txtTitle.setTextSize((float)Variable.titleTextSize);
        txtDescription = (TextView) rootView.findViewById(R.id.txtDescription);
        txtDescription.setTextSize((float)Variable.subTitleTextSize);

        imgNews = (ImageView) rootView.findViewById(R.id.imgNews);
        cardView = (CardView)rootView.findViewById(R.id.cardview);
        listView = (ListView) rootView.findViewById(R.id.listHotNews);
        hotNews.readInternal(fileName,getActivity());
        cardView.setBackgroundColor(Color.parseColor(Variable.backgroundColor));
        listView.setBackgroundColor(Color.parseColor(Variable.backgroundColor));
        //Toast.makeText(getActivity(), newsContents.size()+"", Toast.LENGTH_SHORT).show();

        txtTitle.setText(hotNews.getTitle());

        //xuat ra man hinh

        txtDescription.setText(hotNews.getDescription());
        Picasso.with(getActivity())
                .load(hotNews.getImageLink())
                .placeholder(R.drawable.loading)
                .error(R.drawable.error)
                .into(imgNews);

        adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,hotNews.getSubTitle());
        listView.setAdapter(adapter);
        //final ProgressDialog progressDialog = new ProgressDialog(getActivity());


//            //xử lý khi lần đầu tiên không có dữ liệu file
//            if (hotNews.getSubTitle().size()==0){
//                progressDialog.setMessage("Loading...");
//                progressDialog.show();
//            }
        if (hotNews.getSubTitle().size()==0 && isOnline() == false){
            Toast.makeText(getActivity(), "Chưa lưu tin hot news để hiện lên", Toast.LENGTH_SHORT).show();
        }
        if (hotNews.getSubTitle().size()!=0){
            progressDialog.dismiss();
        }
        if (hotNews.getSubTitle().size()!=0 || isOnline() == false){
            progressDialog.dismiss();
        }
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                progressDialog.dismiss();
                Toast.makeText(context, "receive", Toast.LENGTH_SHORT).show();
                //String s = intent.getStringExtra(HotNewsService.MESSAGE);
                // do something here.
                UpdateUI();
            }
        };




        //OnClick
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //timer.cancel();
                Intent intent = new Intent(getActivity(), ItemContentActivity.class);
                intent.putExtra("Link",hotNews.getLink());
                intent.putExtra("Title",hotNews.getTitle());
                intent.putExtra("Type","Hot News");
                intent.putExtra("Index",-1);
                getActivity().startActivity(intent);
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Toast.makeText(getActivity(),"bcd", Toast.LENGTH_SHORT).show();

                //timer.cancel();
                Intent intent = new Intent(getActivity(), ItemContentActivity.class);
                intent.putExtra("Link",hotNews.getSubLink().get(position));
                intent.putExtra("Title",hotNews.getSubTitle().get(position));
                //intent.putExtra("Object", newsContents.get(position));
                intent.putExtra("Type","Hot News");
                intent.putExtra("Index",position);
                getActivity().startActivity(intent);
            }
        });
        return rootView;
    }

    private void UpdateUI() {

        hotNews.readInternal(fileName,getActivity());

        //Toast.makeText(getActivity(), newsContents.size()+"", Toast.LENGTH_SHORT).show();

        txtTitle.setText(hotNews.getTitle());

        //xuat ra man hinh

        txtDescription.setText(hotNews.getDescription());
        Picasso.with(getActivity())
                .load(hotNews.getImageLink())
                .placeholder(R.drawable.loading)
                .error(R.drawable.error)
                .into(imgNews);
        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,hotNews.getSubTitle());
        //listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onStop() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        progressDialog.dismiss();
        //adapter.notifyDataSetChanged();
        //Toast.makeText(getActivity(), newsContents.size()+"", Toast.LENGTH_SHORT).show();
        UpdateUI();
    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver((receiver),
                new IntentFilter(HotNewsService.REQUEST)
        );
    }
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

}
