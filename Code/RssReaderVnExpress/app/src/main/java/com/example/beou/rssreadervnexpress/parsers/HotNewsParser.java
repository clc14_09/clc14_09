package com.example.beou.rssreadervnexpress.parsers;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.beou.rssreadervnexpress.components.HotNews;
import com.example.beou.rssreadervnexpress.components.NewsContent;
import com.example.beou.rssreadervnexpress.variable.Variable;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.beou.rssreadervnexpress.variable.Variable.fileName;

/**
 * Created by BeoU on 11/25/2016.
 */

public class HotNewsParser extends AsyncTask<Void, Void, Void> {

    private HotNews hotNews = null;
    private Context context;
    ArrayList<NewsContent> hotNewsContents = new ArrayList<NewsContent>();

    public HotNewsParser(Context context) {
        this.context = context;

    }

    public String getHTML()
    {
        HttpResponse response = null;
        HttpGet httpGet = null;
        HttpClient mHttpClient = null;
        String s = "";

        try {
            if(mHttpClient == null){
                mHttpClient = new DefaultHttpClient();
            }

            httpGet = new HttpGet("http://vnexpress.net/");


            response = mHttpClient.execute(httpGet);
            s = EntityUtils.toString(response.getEntity(), "UTF-8");
            //tmp = s.substring(s.indexOf("box_sub_hot_news"));


        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }
    @Override
    protected void onPreExecute() {
//        progressDialog.show();
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        hotNews.saveInternal(fileName,context);

        //Toast.makeText(context, "SAVE FILE.....", Toast.LENGTH_SHORT).show();
        //isSaveDone = true;
        //TODO: Save nội dung của từng bài trong HotNews
        Thread thread = new Thread(){
            @Override
            public void run(){
                try{
                    for (int i=0; i<hotNewsContents.size()-1;i++){
                        hotNewsContents.get(i).saveInternal(createFileName(i),context);
                    }
                    // save hotnew chính vơi tên file +index = -1
                    hotNewsContents.get(hotNewsContents.size()-1).saveInternal(createFileName(-1),context);
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        thread.start();

//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            public void run() {
//                isSaveDone = true;
//            }
//        }, 5000);


//        try {
//            thread.join();
//            Toast.makeText(context, "DONE", Toast.LENGTH_SHORT).show();
//            isSaveDone = true;
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }



    }
    @Override
    protected Void doInBackground(Void... voids) {
        hotNews = getHotNews();
        return null;
    }

    public HotNews getHotNews()
    {
        String html= getHTML();
        Document doc;
        String title = null,imageLink=null, description=null,link=null;
        ArrayList<String> subTitle = new ArrayList<String>();
        ArrayList<String> subLink = new ArrayList<String>();


        //doc = Jsoup.connect("http://vnexpress.net/").get();
        doc = Jsoup.parse(html);
        //lay hot news chinh
        Elements content = doc.select("div[class=box_hot_news]");

        for (Element tmp:content)
        {
            //title
            //System.out.println(tmp.toString());

            title = tmp.select("[class=title_news]").text();
            description = tmp.select("[class=news_lead]").text();

            Element linkContainer = tmp.child(0).child(0);
            link = linkContainer.attr("href");
            imageLink  = linkContainer.child(0).attr("src");
        }


        //lay cac sub hot news
        content = doc.select("[class=box_sub_hot_news]").select("[class=title_news]");

        Log.d("Debug",content.toString());
        for (Element tmp:content)
        {
            subTitle.add(tmp.text());
            String tmpLink = tmp.child(0).attr("href");
            subLink.add(tmpLink);
            //Log.d("abc1",tmp.toString());
            hotNewsContents.add(getContentFromHTML(tmpLink));

        }
        hotNewsContents.add(getContentFromHTML(link));
        return new HotNews(title,imageLink,description,link,subTitle, subLink);

    }
    public NewsContent getContentFromHTML(String pageUrl)
    {
        Document doc;
        String title=null;
        String subTitle =null;
        String date=null;
        ArrayList<String> string = new ArrayList<String>();

        //0: text thuong, 1: link img, 2: text mo ta hinh
        ArrayList<Integer> type = new ArrayList<Integer>();
        try {

            doc = Jsoup.connect(pageUrl).get();
            //lấy tất cả text nằm trong nội dung của mẩu tin
            if (doc.select("[class=block_timer left txt_666]").size()>=2) {
                date = doc.select("[class=block_timer left txt_666]").get(1).text();
            }

            if (doc.select("[class=short_intro txt_666]").size()!=0) {
                subTitle = doc.select("[class=short_intro txt_666]").get(0).text();
            }
            //title = doc.select("[class=short_intro txt_666]").get(0).text();
            if(doc.select("[class=title_news]").size()!=0) {
                title = doc.select("[class=title_news]").get(0).text();
            }
            Elements content = doc.select("div[class=main_content_detail width_common] p, div[class=main_content_detail width_common] img");

            for (Element tmp:content)
            {
                if (tmp.nodeName().equals("img")){
                    //System.out.println(tmp.toString());
                    if ((tmp.hasAttr("data-natural-width"))
                            ||(tmp.hasAttr("data-reference-id"))
                            ||(tmp.hasAttr("data-width"))) {
                        string.add(tmp.attr("src"));
                        type.add(Variable.IMG_LINK);
                    }

                }

                else if (tmp.nodeName().equals("p")){
                    //if(tmp.parent().select("div[class=desc_cation]").toString();
                    //System.out.println(tmp.parent().select("div[class=desc_cation]"));

                    if (tmp.attr("class").equals("Image")){
                        string.add(tmp.text());
                        type.add(Variable.IMG_TEXT);
                    }
                    else {
                        int count = 0;
                        Elements parents = tmp.parent().select("div[class=desc_cation]");
                        for (Element tmp1:parents)
                        {
                            count++;
                            if (tmp1.hasAttr("style")){
                                if (!tmp.text().equals(string.get(string.size()-1))){
                                    string.add(tmp.text());
                                    type.add(Variable.NORMAL_TEXT);
                                }
                            }
                            else{
                                string.add(tmp.text());
                                type.add(Variable.IMG_TEXT);
                            }
                        }
                        if (count == 0){
                            string.add(tmp.text());
                            type.add(Variable.NORMAL_TEXT);
                        }
                    }
                }

            }


        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
        return new NewsContent(title, subTitle,date,string,type);
    }

    private String createFileName(int index){
        return "HotNews"+index;
    }
}
