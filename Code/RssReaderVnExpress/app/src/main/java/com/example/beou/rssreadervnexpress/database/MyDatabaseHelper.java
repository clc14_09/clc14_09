package com.example.beou.rssreadervnexpress.database;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.beou.rssreadervnexpress.components.NewsContent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MyDatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "SQLite";


    // Phiên bản
    private static final int DATABASE_VERSION = 1;


    // Tên cơ sở dữ liệu.
    private static final String DATABASE_NAME = "NewsContent_Manager";

    // Tên bảng: NewsContent.
    private static final String TABLE_NEWS_CONTENT = "NewsContent_Content";

    private static final String COLUMN_NEWS_TITLE ="NewsContent_Title";
    private static final String COLUMN_NEWS_SUBTITLE ="NewsContent_Subtitle";
    private static final String COLUMN_NEWS_DATE = "NewsContent_Date";
    private static final String COLUMN_NEWS_TEXT = "NewsContent_Text";
    private static final String COLUMN_NEWS_TYPE= "NewsContent_Type";
    //private static final String COLUMN_NEWS_IMAGE = "NewsContent_Image";

    public MyDatabaseHelper(Context context)  {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Tạo các bảng.
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(TAG, "MyDatabaseHelper.onCreate ... ");


        // Script tạo bảng.
        String script = "CREATE TABLE " + TABLE_NEWS_CONTENT + "("
                + COLUMN_NEWS_TITLE + " TEXT PRIMARY KEY,"
                + COLUMN_NEWS_SUBTITLE + " TEXT,"
                + COLUMN_NEWS_DATE + " TEXT"
                + COLUMN_NEWS_TEXT + " TEXT"
                + COLUMN_NEWS_TYPE + " TEXT"
                //+ COLUMN_NEWS_IMAGE + " TEXT"
                + ")";
        // Chạy lệnh tạo bảng.
        db.execSQL(script);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        Log.i(TAG, "MyDatabaseHelper.onUpgrade ... ");

        // Hủy (drop) bảng cũ nếu nó đã tồn tại.
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NEWS_CONTENT);


        // Và tạo lại.
        onCreate(db);
    }



    public void addNewsContent(NewsContent news) throws JSONException {
        //Log.i(TAG, "MyDatabaseHelper.addNewsContent ... " + news.getNewsContentTitle());

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_NEWS_TITLE, news.getTitle());
        values.put(COLUMN_NEWS_SUBTITLE, news.getSubTitle());
        values.put(COLUMN_NEWS_DATE, news.getDate());

        JSONObject json = new JSONObject();
        json.put("stringArray", new JSONArray(news.getString()));
        String stringArray = json.toString();
        values.put(COLUMN_NEWS_TEXT, stringArray);

        json.put("typeArray", new JSONArray(news.getType()));
        String typeArray = json.toString();
        values.put(COLUMN_NEWS_TYPE, typeArray);

//        json = new JSONObject();
//        json.put("imageArray", new JSONArray(news.get()));
//        String imageArray = json.toString();
//        values.put(COLUMN_NEWS_IMAGE, imageArray);



        // Chèn một dòng dữ liệu vào bảng.
        db.insert(TABLE_NEWS_CONTENT, null, values);


        // Đóng kết nối database.
        db.close();
    }


//    public NewsContent getNewsContent(int id) {
//        Log.i(TAG, "MyDatabaseHelper.getNewsContent ... " + id);
//
//        SQLiteDatabase db = this.getReadableDatabase();
//
//        Cursor cursor = db.query(TABLE_NEWS_CONTENT, new String[] {COLUMN_NEWS_TITLE,
//                        COLUMN_NOTE_TITLE
//                        , COLUMN_NEWS_DATE}, COLUMN_NEWS_TITLE + "=?"
//                        , new String[] { String.valueOf(id) }, null, null, null, null);
//        if (cursor != null)
//            cursor.moveToFirst();
//
//        NewsContent news = new NewsContent(Integer.parseInt(cursor.getString(0)),
//                cursor.getString(1), cursor.getString(2));
//        // return news
//        return news;
//    }


    public List<NewsContent> getAllNewsContents() throws JSONException {
        Log.i(TAG, "MyDatabaseHelper.getAllNewsContents ... " );

        List<NewsContent> newsList = new ArrayList<NewsContent>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NEWS_CONTENT;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // Duyệt trên con trỏ, và thêm vào danh sách.
        if (cursor.moveToFirst()) {
            do {
                NewsContent news = new NewsContent();
                news.setTitle(cursor.getString(0));
                news.setSubTitle(cursor.getString(1));
                news.setDate(cursor.getString(2));

                JSONObject json = null;
                try {
                    json = new JSONObject(cursor.getString(3));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JSONArray jStringArr = json.optJSONArray("stringArray");
                ArrayList<String> stringArr = new ArrayList<String>();
                if (jStringArr != null) {
                    for (int i=0;i<jStringArr.length();i++){
                        stringArr.add(jStringArr.getString(i));
                    }
                }
                news.setString(stringArr);

                json = new JSONObject(cursor.getString(4));
                JSONArray jTypeArr = json.optJSONArray("typeArray");
                ArrayList<Integer> typeArr = new ArrayList<Integer>();
                if (jTypeArr != null) {
                    for (int i=0;i<jTypeArr.length();i++){
                        typeArr.add(jTypeArr.getInt(i));
                    }
                }
                news.setType(typeArr);

                // Thêm vào danh sách.
                newsList.add(news);
            } while (cursor.moveToNext());
        }

        // return news list
        return newsList;
    }

    public int getNewsContentCount() {
        Log.i(TAG, "MyDatabaseHelper.getNewsContentsCount ... " );

        String countQuery = "SELECT  * FROM " + TABLE_NEWS_CONTENT;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();

        cursor.close();

        // return count
        return count;
    }




    public void deleteNewsContent(NewsContent news) {
        //Log.i(TAG, "MyDatabaseHelper.updateNewsContent ... " + news.getNewsContentTitle() );

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NEWS_CONTENT, COLUMN_NEWS_TITLE + " = ?",
                new String[] { String.valueOf(news.getTitle()) });
        db.close();
    }
}
