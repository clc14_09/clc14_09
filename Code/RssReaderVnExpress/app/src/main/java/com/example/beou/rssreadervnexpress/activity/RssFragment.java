package com.example.beou.rssreadervnexpress.activity;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.beou.rssreadervnexpress.R;
import com.example.beou.rssreadervnexpress.parsers.RssXMLParser;
import com.example.beou.rssreadervnexpress.variable.Variable;

/**
 * Created by BeoU on 12/18/2016.
 */

public class RssFragment extends Fragment {
    public static final String RSS_POSITION = "rssPos";
    RecyclerView recyclerView;
    public RssFragment() {
        // Empty constructor required for fragment subclasses
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.rss_list_fragment, container, false);
        int index = getArguments().getInt(RSS_POSITION);
        recyclerView= (RecyclerView) rootView.findViewById(R.id.recyclerview);
        RssXMLParser rssXMLParser=new RssXMLParser(index,getActivity(),recyclerView);
        rssXMLParser.execute();
        return rootView;
    }
}
