package com.example.beou.rssreadervnexpress.activity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.example.beou.rssreadervnexpress.R;
import com.example.beou.rssreadervnexpress.components.NewsContent;
import com.example.beou.rssreadervnexpress.database.BookMarkDB;
import com.example.beou.rssreadervnexpress.parsers.RssHTMLParser;
import com.example.beou.rssreadervnexpress.variable.Variable;

public class ItemContentActivity extends Activity {
    LinearLayout linearLayout;
    ScrollView scrollView;
    BookMarkDB DB;
    String link = null;
    String title = null;
    Menu curMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_content_activity);
        DB = new BookMarkDB(this);

        linearLayout = (LinearLayout) findViewById(R.id.activity_item_content);
        scrollView = (ScrollView) findViewById(R.id.ScrollView01);

        scrollView.setBackgroundColor(Color.parseColor(Variable.backgroundColor));

        link = getIntent().getStringExtra("Link");
        title = getIntent().getStringExtra("Title");
        //NewsContent newsContent = (NewsContent) getIntent().getSerializableExtra("Object");
        String type = getIntent().getStringExtra("Type");

        Integer index = getIntent().getIntExtra("Index", -2);
        if (type != null){
            if (type.equals("Hot News")){
                if (index!=-2){
                    new NewsContent().displayOffline(createFileName(index),this,linearLayout);
                }
                else {
                    Toast.makeText(this, "NULL", Toast.LENGTH_SHORT).show();
                }
            }
        }
        else{
            RssHTMLParser rssHTMLParser = new RssHTMLParser(this,linearLayout);
            rssHTMLParser.execute(link);
        }
    }
    private String createFileName(Integer index){
        return "HotNews"+index;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.bookmark_menu, menu);
        curMenu = menu;
        //Toast.makeText(this, title, Toast.LENGTH_SHORT).show();
        if (DB.findIndex(title)==-1) {// chua có trong bookmark
            curMenu.findItem(R.id.action_bookmark_checked).setVisible(false);
        }
        else{
            curMenu.findItem(R.id.action_bookmark).setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_bookmark:
                Toast.makeText(this, "Insert", Toast.LENGTH_SHORT).show();
                DB.Insert(title,link);
                curMenu.findItem(R.id.action_bookmark).setVisible(false);
                curMenu.findItem(R.id.action_bookmark_checked).setVisible(true);
                return true;
            case R.id.action_bookmark_checked:
                Toast.makeText(this, "Delete", Toast.LENGTH_SHORT).show();
                int index = DB.findIndex(title);
                if (index!=-1) {
                    DB.Delete(DB.findIndex(title));
                }
                curMenu.findItem(R.id.action_bookmark).setVisible(true);
                curMenu.findItem(R.id.action_bookmark_checked).setVisible(false);
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }
//    }
    @Override
    protected void onDestroy() {
        DB.Save();
        super.onDestroy();
    }
}
