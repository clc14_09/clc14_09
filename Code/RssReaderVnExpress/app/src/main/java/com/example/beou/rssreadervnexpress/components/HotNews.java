package com.example.beou.rssreadervnexpress.components;

import android.content.Context;

import com.example.beou.rssreadervnexpress.variable.Variable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by BeoU on 12/2/2016.
 */

public class HotNews {
    String title,imageLink, description,link;
    ArrayList<String> subTitle = new ArrayList<String>();
    ArrayList<String> subLink = new ArrayList<String>();


    public HotNews(String title, String imageLink, String description, String link, ArrayList<String> subTitle, ArrayList<String> subLink) {
        this.title = title;
        this.imageLink = imageLink;
        this.description = description;
        this.link = link;
        this.subTitle = subTitle;
        this.subLink = subLink;
    }

    public HotNews() {

    }

    public String getTitle() {
        return title;
    }

    public String getImageLink() {
        return imageLink;
    }

    public String getDescription() {
        return description;
    }

    public String getLink() {
        return link;
    }

    public ArrayList<String> getSubTitle() {
        return subTitle;
    }

    public ArrayList<String> getSubLink() {
        return subLink;
    }
    public void saveInternal(String fileName, Context context) {
        File file;
        FileOutputStream outputStream;
        try {
            file = new File(context.getFilesDir(), fileName);
            outputStream = new FileOutputStream(file);

            outputStream.write((title+"\n").getBytes());
            outputStream.write((imageLink+"\n").getBytes());
            outputStream.write((description+"\n").getBytes());
            outputStream.write((link+"\n").getBytes());

            int size = subTitle.size();
            outputStream.write((size+"\n").getBytes());

            for (int i=0; i<size;i++){
                outputStream.write((subTitle.get(i)+"\n").getBytes());
                outputStream.write((subLink.get(i)+"\n").getBytes());
            }

            outputStream.close();
            Variable.isSaveDone = true;
        } catch (IOException e) {
            e.printStackTrace();

        }

    }
    public void readInternal(String fileName, Context context){
        BufferedReader input = null;
        File file = null;
        try {
            file = new File(context.getFilesDir(), fileName); // Pass getFilesDir() and "MyFile" to read file

            input = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String line;
            subTitle.clear();
            line = input.readLine();
            if (line == null){
                return;
            }
            title = line;
            imageLink= input.readLine();
            description= input.readLine();
            link= input.readLine();
            int size = Integer.parseInt(input.readLine());
            for (int i=0;i<size;i++){
                subTitle.add(input.readLine());
                subLink.add(input.readLine());
            }
            input.close();
        } catch (IOException e) {
            e.printStackTrace();

        }
    }
}
