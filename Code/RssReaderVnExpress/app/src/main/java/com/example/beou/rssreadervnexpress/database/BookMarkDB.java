package com.example.beou.rssreadervnexpress.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.File;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;


/**
 * Created by Quang Tu on 11/18/2016.
 */

public class BookMarkDB{
    private ArrayList<String> Title;
    private ArrayList<String> Link;
    private Integer numOfArray;
    private SQLiteDatabase db;
    Context context;
    public BookMarkDB(Context context) {
        this.context = context;
        File file = context.getDatabasePath("BookMark");
        this.Title = new ArrayList<String>();
        this.Link = new ArrayList<String>();
        if (file.exists()) {
            numOfArray = 0;
            db = context.openOrCreateDatabase("BookMark", MODE_PRIVATE, null);
            Cursor c = db.rawQuery("SELECT * FROM " + "BookMark", null);
            if(c.getCount() <=0)
                return;
            int Column1 = c.getColumnIndex("Title");
            int Column2 = c.getColumnIndex("Link");
            c.moveToFirst();
            //Get data from database
            do {
                String Title = c.getString(Column1);
                String Link = c.getString(Column2);
                this.Title.add(Title);
                this.Link.add(Link);
                ++numOfArray;
            }while(c.moveToNext());

            db.execSQL("delete from BookMark");
        }
        else
        {
            db = context.openOrCreateDatabase("BookMark", MODE_PRIVATE, null);
            db.execSQL("CREATE TABLE IF NOT EXISTS "
                    + "BookMark"
                    + " (Title TEXT, Link TEXT);");
            numOfArray=0;
        }
    }
    //Insert data
    public void Insert(String NewTitle, String NewLink)
    {
        this.Title.add(NewTitle);
        this.Link.add(NewLink);
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        //String date = sdf.format(NewDate);
        ++numOfArray;
    }
    public int findIndex(String title){
        for (int i=0; i<Title.size();i++){
            if (title.equals(Title.get(i))){
                return i;
            }
        }
        return -1;
    }
    public void Delete(int id)
    {
        if(this.Title.isEmpty())
            return;
        else
        {
            Title.remove(id);
            Link.remove(id);
        }
        --numOfArray;
    }
    //Save data into Database
    public void Save()
    {
//        db.delete("BookMark",null,null);
        for(int i=0;i<this.numOfArray;++i)
        {
            //Check if record already exists
            ContentValues insertValues = new ContentValues();
            insertValues.put("Title",this.Title.get(i));
            insertValues.put("Link",this.Link.get(i));
            db.insert("BookMark",null,insertValues);
        }
    }

    public String GetTitle(int i)
    {
        if(this.Title.isEmpty())
            return null;
        return this.Title.get(i);
    }
    public String GetLink(int i)
    {
        if(this.Title.isEmpty())
            return null;
        return this.Link.get(i);
    }
    public int GetNumArray()
    {
        return this.numOfArray;
    }
}


